﻿CREATE TABLE [dbo].[Users]
(
	[Id] INT IDENTITY(1,1) NOT NULL, 
    [Email] VARCHAR(128) NOT NULL, 
    [IsEmailConfirmed] BIT NOT NULL CONSTRAINT [DF_Users_IsEmailConfirmed] DEFAULT 0, 
    [EmailConfirmToken] VARCHAR(256) NULL, 
    [EmailConfirmTokenExpiresAt] DATETIME NULL, 
    [Firstname] VARCHAR(128) NOT NULL,
    [Lastname] VARCHAR(128) NULL,
    [RegisteredAt] DATETIME NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([Id])
)
