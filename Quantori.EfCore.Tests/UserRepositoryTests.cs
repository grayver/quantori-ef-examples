﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Quantori.EfCore.Example;
using Quantori.EfCore.Example.Entities;
using Quantori.EfCore.Example.Repositories;
using System.Linq;
using Xunit;

namespace Quantori.EfCore.Tests
{
    public class UserRepositoryTests
    {
        private readonly DbContextOptions<EfCoreExampleContext> _contextOptions;

        public UserRepositoryTests()
        {
            _contextOptions = new DbContextOptionsBuilder<EfCoreExampleContext>()
                .UseInMemoryDatabase("QuantoriEfInMemory")
                .ConfigureWarnings(wcb => wcb.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                .Options;
        }

        [Fact]
        public async void GetByIdAsyncV1()
        {
            using (var context = new EfCoreExampleContext(_contextOptions))
            {
                // populate users if needed
                if (!context.Users.Any())
                {
                    context.Users.Add(new User { Id = 1, Email = "test@test.test", Firstname = "Test User" });
                    context.SaveChanges();
                }

                var userRepo = new UserRepository(context);

                var user = await userRepo.GetByIdAsyncV1(1, 2);

                Assert.NotNull(user);
                Assert.Equal("test@test.test", user.Email);
                Assert.Equal("Test User", user.Firstname);

                user = await userRepo.GetByIdAsyncV1(2, 1);

                Assert.Null(user);
            }
        }
    }
}
