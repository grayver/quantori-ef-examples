﻿using Microsoft.EntityFrameworkCore;
using Quantori.EfCore.Example.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quantori.EfCore.Example.Repositories
{
    public class PostRepository
    {
        private readonly EfCoreExampleContext _context;

        public PostRepository(EfCoreExampleContext context)
        {
            _context = context;
        }

        public async Task<Post> GetByIdAsync(int id)
        {
            return await _context.Posts.SingleOrDefaultAsync(p => p.Id == id);
        }

        public async Task<List<Article>> GetRandomArticlesAsyncV1(int count)
        {
            return await _context.Articles
                .OrderBy(a => Guid.NewGuid())
                .Take(count)
                .ToListAsync();
        }

        public async Task<List<Article>> GetRandomArticlesAsyncV2(int count)
        {
            return await _context.Articles
                .OrderBy(a => Math.Sin(a.Id))
                .Take(count)
                .ToListAsync();
        }

        public async Task<List<Post>> GetRecommendedPostsByUserAsync(int userId, int count)
        {
            // take recently liked articles
            var likedArticles = await _context.Articles
                .Join(_context.PostGrades, a => a.Id, pg => pg.PostId,
                    (a, pg) => new { Article = a, Grade = pg })
                .Where(item => item.Grade.UserId == userId && item.Grade.Value > 0)
                .OrderByDescending(item => item.Grade.CreatedAt)
                .Select(item => item.Article)
                .Take(count)
                .ToListAsync();

            var likedArticleGenres = likedArticles.Select(a => a.Genre.ToLower()).Distinct().ToArray();

            // search for unread similar articles by genre
            var similarToLikedByGenre = await _context.Articles
                .Where(a => likedArticleGenres.Contains(a.Genre.ToLower())
                    && !_context.ReadFlags.Any(pg => pg.PostId == a.Id && pg.UserId == userId))
                .OrderByDescending(a => a.CreatedAt)
                .Take(count)
                .ToListAsync();

            // take recently liked reviews
            var likedReviews = await _context.Reviews
                .Join(_context.PostGrades, r => r.Id, pg => pg.PostId,
                    (r, pg) => new { Review = r, Grade = pg })
                .Where(item => item.Grade.UserId == userId && item.Grade.Value > 0)
                .OrderByDescending(item => item.Grade.CreatedAt)
                .Select(item => item.Review)
                .Take(count)
                .ToListAsync();

            var likedReviewSubjects = likedReviews.Select(r => r.Subject.ToLower()).Distinct().ToArray();

            // search for unread similar reviews by subject
            var similarToLikedBySubject = await _context.Reviews
                .Where(r => likedReviewSubjects.Contains(r.Subject.ToLower())
                    && !_context.ReadFlags.Any(pg => pg.PostId == r.Id && pg.UserId == userId))
                .OrderByDescending(r => r.CreatedAt)
                .Take(count)
                .ToListAsync();

            return similarToLikedByGenre
                .Cast<Post>()
                .Union(similarToLikedByGenre)
                .OrderBy(p => Guid.NewGuid())
                .Take(count)
                .ToList();
        }

        public async Task InsertArticleAsync(Article article)
        {
            await _context.Articles.AddAsync(article);
        }

        public async Task InsertReviewAsync(Review review)
        {
            await _context.Reviews.AddAsync(review);
        }

        public async Task UpdateLikeCountAsync(int postId, int likeCountChange)
        {
            var postEntry = _context.ChangeTracker.Entries<Post>()
                .SingleOrDefault(e => e.Entity.Id == postId);

            await _context.Database.ExecuteSqlRawAsync(
                sql: @"UPDATE Posts SET LikeCount = LikeCount + {0} WHERE Id = {1}",
                parameters: new object[] { likeCountChange, postId });

            if (postEntry != null)
                await postEntry.ReloadAsync();
        }
    }
}
