﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Quantori.EfCore.Example.Repositories
{
    public class UserRatingRepository
    {
        private readonly EfCoreExampleContext _context;

        public UserRatingRepository(EfCoreExampleContext context)
        {
            _context = context;
        }

        public Task UpdateRatingsAsync()
        {
            return _context.Database.ExecuteSqlRawAsync(
                sql: @"
UPDATE Ur
SET
    Value = Positions.Rating,
    PreviousValue = Ur.Value,
    Position = Positions.Position,
    PreviousPosition = Ur.Position,
    PositionChange = Ur.Position - Positions.Position,
    PreviousCalculatedAt = GETDATE()
FROM UserRatings Ur
INNER JOIN
(
    SELECT
        Ratings.AuthorId,
        Ratings.Rating,
        ROW_NUMBER() OVER(ORDER BY Ratings.Rating DESC, Ratings.LastGradeGotAt ASC) AS Position
    FROM
    (
        SELECT
            Pst.AuthorId,
            SUM(Pg.Value) AS Rating,
            MAX(Pg.CreatedAt) AS LastGradeGotAt
        FROM PostGrades Pg
        INNER JOIN Posts Pst
            ON Pst.Id = Pg.PostId
        GROUP BY
            Pst.AuthorId
    ) Ratings
) Positions
	ON Positions.AuthorId = Ur.UserId");
        }
    }
}
