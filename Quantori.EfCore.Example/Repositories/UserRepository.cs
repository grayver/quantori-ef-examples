﻿using Microsoft.EntityFrameworkCore;
using Quantori.EfCore.Example.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quantori.EfCore.Example.Repositories
{
    public class UserRepository
    {
        private readonly EfCoreExampleContext _context;

        public UserRepository(EfCoreExampleContext context)
        {
            _context = context;
        }

        public Task<User> GetByEmailAsync(string email)
        {
            var emailLower = email.ToLower();
            return _context.Users.SingleOrDefaultAsync(u => u.Email.ToLower() == emailLower);
        }

        public async Task<User> GetByIdAsyncV1(int id, int requestUserId)
        {
            var user = await _context.Users.SingleOrDefaultAsync(u => u.Id == id);
            if (user != null)
            {
                user.IsFriend = await _context.UserFriends.AnyAsync(uf => uf.FriendId == id && uf.UserId == requestUserId);
                user.IsBlocked = await _context.UserBlocks.AnyAsync(ub => ub.BlockId == id && ub.UserId == requestUserId);
            }

            return user;
        }

        public async Task<User> GetByIdAsyncV2(int id, int requestUserId)
        {
            var user = await _context.Users.Include(u => u.UserFriends).SingleOrDefaultAsync(u => u.Id == id);
            if (user != null)
            {
                user.IsFriend = user.UserFriends.Any(uf => uf.FriendId == requestUserId);
                user.IsBlocked = await _context.UserBlocks.AnyAsync(ub => ub.BlockId == id && ub.UserId == requestUserId);
            }

            return user;
        }

        public async Task<User> GetByIdAsyncV3(int id, int requestUserId)
        {
            // complex LINQ query to make one SQL query at once
            var query = from user in _context.Users
                        join uf in _context.UserFriends.Where(uf => uf.FriendId == requestUserId)
                            on user.Id equals uf.UserId
                            into ufJoined
                        from ufj in ufJoined.DefaultIfEmpty()
                        join ub in _context.UserBlocks.Where(ub => ub.UserId == requestUserId)
                            on user.Id equals ub.BlockId
                            into ubJoined
                        from ubj in ubJoined.DefaultIfEmpty()
                        where
                            user.Id == id
                        select new
                        {
                            User = user,
                            IsFriend = ufj != null,
                            IsBlocked = ubj != null
                        };
            var userAggr = await query.SingleOrDefaultAsync();

            if (userAggr != null)
            {
                userAggr.User.IsFriend = userAggr.IsFriend;
                userAggr.User.IsBlocked = userAggr.IsBlocked;
            }

            return userAggr?.User;
        }

        public async Task<List<Permission>> GetPermissionsByUserAsync(int userId)
        {
            return await _context.Users
                .Where(u => u.Id == userId)
                .SelectMany(u => u.UserRoles.SelectMany(ur => ur.Role.RolePermissions))
                .Select(rp => rp.Permission)
                .Distinct()
                .ToListAsync();
        }
    }
}
