﻿using Microsoft.EntityFrameworkCore;
using Quantori.EfCore.Example.Entities;
using System.Threading.Tasks;

namespace Quantori.EfCore.Example.Repositories
{
    public class PostGradeRepository
    {
        private readonly EfCoreExampleContext _context;

        public PostGradeRepository(EfCoreExampleContext context)
        {
            _context = context;
        }

        public Task<PostGrade> GetAsync(int postId, int userId)
        {
            return _context.PostGrades.SingleOrDefaultAsync(pg => pg.PostId == postId && pg.UserId == userId);
        }

        public async Task InsertAsync(PostGrade postGrade)
        {
            await _context.PostGrades.AddAsync(postGrade);
        }
    }
}
