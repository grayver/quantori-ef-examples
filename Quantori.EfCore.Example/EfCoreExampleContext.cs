﻿using Microsoft.EntityFrameworkCore;
using Quantori.EfCore.Example.Entities;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Quantori.EfCore.Example
{
    public class EfCoreExampleContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<RolePermission> RolePermissions { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<UserFriend> UserFriends { get; set; }
        public DbSet<UserBlock> UserBlocks { get; set; }
        public DbSet<UserRating> UserRatings { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<ReadFlag> ReadFlags { get; set; }
        public DbSet<PostGrade> PostGrades { get; set; }

        public EfCoreExampleContext(DbContextOptions<EfCoreExampleContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .ToTable("Users");

            modelBuilder.Entity<User>()
                .HasIndex(u => u.Email)
                .IsUnique();

            modelBuilder.Entity<Role>()
                .ToTable("Roles");

            modelBuilder.Entity<Role>()
                .HasIndex(r => r.Name)
                .IsUnique();

            modelBuilder.Entity<Permission>()
                .ToTable("Permissions");

            modelBuilder.Entity<Permission>()
                .HasIndex(p => p.Name)
                .IsUnique();

            modelBuilder.Entity<RolePermission>()
                .ToTable("RolePermissions");

            modelBuilder.Entity<RolePermission>()
                .HasKey(rp => new { rp.RoleId, rp.PermissionId });

            modelBuilder.Entity<RolePermission>()
                .HasOne(rp => rp.Role)
                .WithMany(r => r.RolePermissions)
                .HasForeignKey(rp => rp.RoleId);

            modelBuilder.Entity<RolePermission>()
                .HasOne(rp => rp.Permission)
                .WithMany()
                .HasForeignKey(rp => rp.PermissionId);

            modelBuilder.Entity<UserRole>()
                .ToTable("UserRoles");

            modelBuilder.Entity<UserRole>()
                .HasKey(ur => new { ur.UserId, ur.RoleId });

            modelBuilder.Entity<UserRole>()
                .HasOne(ur => ur.User)
                .WithMany(u => u.UserRoles)
                .HasForeignKey(ur => ur.UserId);

            modelBuilder.Entity<UserRole>()
                .HasOne(ur => ur.Role)
                .WithMany(r => r.UserRoles)
                .HasForeignKey(ur => ur.RoleId);

            modelBuilder.Entity<UserFriend>()
                .ToTable("UserFriends");

            modelBuilder.Entity<UserFriend>()
                .HasKey(uf => new { uf.UserId, uf.FriendId });

            modelBuilder.Entity<UserFriend>()
                .HasOne(uf => uf.User)
                .WithMany(u => u.UserFriends)
                .HasForeignKey(uf => uf.UserId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<UserFriend>()
                .HasOne(uf => uf.Friend)
                .WithMany()
                .HasForeignKey(uf => uf.FriendId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<UserBlock>()
                .ToTable("UserBlocks");

            modelBuilder.Entity<UserBlock>()
                .HasKey(ub => new { ub.UserId, ub.BlockId });

            modelBuilder.Entity<UserRating>()
                .ToTable("UserRatings");

            modelBuilder.Entity<UserRating>()
                .HasKey(ur => ur.UserId);

            modelBuilder.Entity<UserRating>()
                .HasOne(ur => ur.User)
                .WithOne(u => u.Rating)
                .HasForeignKey<UserRating>(ur => ur.UserId)
                .HasConstraintName("FK_UserRatings_Users_UserId");

            modelBuilder.Entity<Post>()
                .ToTable("Posts");

            modelBuilder.Entity<Post>()
                .HasDiscriminator(p => p.Type)
                .HasValue<Article>("article")
                .HasValue<Review>("review");

            modelBuilder.Entity<Post>()
                .Ignore(p => p.Author);

            modelBuilder.Entity<Article>()
                .HasOne(a => a.Author)
                .WithMany(u => u.AuthoredArticles)
                .HasForeignKey(a => a.AuthorId)
                .HasConstraintName("FK_Posts_Users_AuthorId");

            modelBuilder.Entity<Review>()
                .HasOne(r => r.Author)
                .WithMany(u => u.AuthoredReviews)
                .HasForeignKey(r => r.AuthorId)
                .HasConstraintName("FK_Posts_Users_AuthorId");

            modelBuilder.Entity<ReadFlag>()
                .ToTable("ReadFlags");

            modelBuilder.Entity<ReadFlag>()
                .HasKey(rf => new { rf.PostId, rf.UserId });

            modelBuilder.Entity<User>()
                .HasMany(u => u.ReadFlags)
                .WithOne(rf => rf.User)
                .HasForeignKey(rf => rf.UserId);

            modelBuilder.Entity<Post>()
                .HasMany(p => p.ReadFlags)
                .WithOne(rf => rf.Post)
                .HasForeignKey(rf => rf.PostId);

            modelBuilder.Entity<PostGrade>()
                .ToTable("PostGrades");

            modelBuilder.Entity<PostGrade>()
                .HasKey(pg => new { pg.PostId, pg.UserId });
        }

        public override int SaveChanges()
        {
            this.AddTrackingTimestamps();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            this.AddTrackingTimestamps();
            return base.SaveChangesAsync(cancellationToken);
        }

        private void AddTrackingTimestamps()
        {
            var entries = ChangeTracker.Entries()
                .Where(e => e.Entity is TimeTrackedEntity && (e.State == EntityState.Added || e.State == EntityState.Modified))
                .ToList();

            foreach (var entry in entries)
            {
                var entity = entry.Entity as TimeTrackedEntity;
                if (entry.State == EntityState.Added)
                    entity.CreatedAt = DateTimeOffset.Now;
                entity.LastModifiedAt = DateTimeOffset.Now;
            }
        }
    }
}
