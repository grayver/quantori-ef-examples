﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Quantori.EfCore.Example.Repositories;
using Quantori.EfCore.Example.Services;
using Serilog;
using System;
using System.Threading.Tasks;

namespace Quantori.EfCore.Example
{
    public class Program
    {
        public static void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<EfCoreExampleContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("RuntimeConnection")));

            services.AddLogging((builder) =>
            {
                builder.AddSerilog(dispose: true);
            });

            // add custom repos and services
            services.AddScoped<PostGradeRepository>();
            services.AddScoped<PostRepository>();
            services.AddScoped<UserRatingRepository>();
            services.AddScoped<UserRepository>();

            services.AddScoped<PostService>();
            services.AddScoped<UserService>();
            services.AddScoped<ExampleService>();
        }

        public static async Task<int> Main(string[] args)
        {
            var configBuilder = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory);
            configBuilder.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = configBuilder.Build();

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();

            var services = new ServiceCollection();
            services.AddSingleton<IConfiguration>(configuration);
            ConfigureServices(services, configuration);
            using (var serviceProvider = services.BuildServiceProvider())
            {
                var exampleService = serviceProvider.GetRequiredService<ExampleService>();
                await exampleService.RunUpdatePostRaceConditionAsync();
            }

            return 0;
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
            => new HostBuilder()
                .ConfigureAppConfiguration((context, config) =>
                {
                    config.SetBasePath(AppContext.BaseDirectory);
                    config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                })
                .ConfigureServices((context, services) =>
                {
                    services.AddDbContext<EfCoreExampleContext>(options =>
                        options.UseSqlServer(context.Configuration.GetConnectionString("DesignConnection")));
                })
                .ConfigureLogging((context, logging) =>
                {
                    logging.AddDebug();
                    logging.AddSerilog(dispose: true);

                    Log.Logger = new LoggerConfiguration()
                        .ReadFrom.Configuration(context.Configuration)
                        .CreateLogger();
                });
    }
}
