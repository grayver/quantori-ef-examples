﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quantori.EfCore.Example.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quantori.EfCore.Example.Services
{
    public class ExampleService
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly ILogger<ExampleService> _logger;

        public ExampleService(
            IServiceScopeFactory serviceScopeFactory,
            ILogger<ExampleService> logger)
        {
            _serviceScopeFactory = serviceScopeFactory;
            _logger = logger;
        }

        public async Task GetUserByEmailAsync()
        {
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var userRepo = scope.ServiceProvider.GetRequiredService<UserRepository>();
                var user = await userRepo.GetByEmailAsync("someone@mail.com");
                if (user != null)
                {
                    _logger.LogInformation("User with Id {0} was found", user.Id);
                }
                else
                {
                    _logger.LogInformation("User was not found");
                }
            }
        }

        public async Task GetUserByIdAsync()
        {
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var userRepo = scope.ServiceProvider.GetRequiredService<UserRepository>();
                var user = await userRepo.GetByIdAsyncV1(1, 2);
                if (user != null)
                {
                    _logger.LogInformation("User with Id {0} was found", user.Id);
                }
                else
                {
                    _logger.LogInformation("User was not found");
                }
            }
        }

        public async Task GetRandomArticlesAsync()
        {
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var postRepo = scope.ServiceProvider.GetRequiredService<PostRepository>();
                var articles = await postRepo.GetRandomArticlesAsyncV1(1);
                _logger.LogInformation("{0} random articles were returned", articles.Count);
            }
        }

        public async Task LikePostAsync()
        {
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var postService = scope.ServiceProvider.GetRequiredService<PostService>();
                await postService.LikeAsyncV1(1, 1);
                _logger.LogInformation("Post liked");
            }
        }

        public async Task GetRecommendedPostsAsync()
        {
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var postRepo = scope.ServiceProvider.GetRequiredService<PostRepository>();
                var posts = await postRepo.GetRecommendedPostsByUserAsync(1, 3);
                foreach (var post in posts)
                    _logger.LogInformation("Recommended post with Id {0}: {1}", post.Id, post.Title);
            }
        }

        public async Task GetUserPermissionsAsync()
        {
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var userRepo = scope.ServiceProvider.GetRequiredService<UserRepository>();
                var permissions = await userRepo.GetPermissionsByUserAsync(3);
                _logger.LogInformation("User has following permissions: {0}", string.Join(',', permissions.Select(p => p.Name)));
            }
        }

        public async Task RunLikeRaceConditionAsync()
        {
            var userId = 1;
            var postId = 1;

            var task1 = Task.Run(async () =>
            {
                using (var scope = _serviceScopeFactory.CreateScope())
                {
                    var postService = scope.ServiceProvider.GetRequiredService<PostService>();
                    await postService.LikeAsyncV1(postId, userId);
                }
            });

            var task2 = Task.Run(async () =>
            {
                using (var scope = _serviceScopeFactory.CreateScope())
                {
                    var postService = scope.ServiceProvider.GetRequiredService<PostService>();
                    await postService.LikeAsyncV1(postId, userId);
                }
            });

            await Task.WhenAll(task1, task2);
        }

        public async Task RunUpdatePostRaceConditionAsync()
        {
            var postId = 1;

            var task1 = Task.Run(async () =>
            {
                using (var scope = _serviceScopeFactory.CreateScope())
                {
                    try
                    {
                        var postRepo = scope.ServiceProvider.GetRequiredService<PostRepository>();
                        var context = scope.ServiceProvider.GetRequiredService<EfCoreExampleContext>();
                        var article = await postRepo.GetByIdAsync(postId);
                        article.LikeCount = 100;
                        await context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        _logger.LogInformation("Task 1 got concurrency exception");
                    }
                }
            });

            var task2 = Task.Run(async () =>
            {
                using (var scope = _serviceScopeFactory.CreateScope())
                {
                    try
                    {
                        var postRepo = scope.ServiceProvider.GetRequiredService<PostRepository>();
                        var context = scope.ServiceProvider.GetRequiredService<EfCoreExampleContext>();
                        var article = await postRepo.GetByIdAsync(postId);
                        article.LikeCount = 200;
                        await context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        _logger.LogInformation("Task 2 got concurrency exception");
                    }
                }
            });

            await Task.WhenAll(task1, task2);
        }
    }
}
