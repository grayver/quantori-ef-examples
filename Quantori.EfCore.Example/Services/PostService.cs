﻿using Microsoft.EntityFrameworkCore;
using Quantori.EfCore.Example.Entities;
using Quantori.EfCore.Example.Repositories;
using System.Data;
using System.Threading.Tasks;

namespace Quantori.EfCore.Example.Services
{
    public class PostService
    {
        private readonly EfCoreExampleContext _context;
        private readonly PostRepository _postRepo;
        private readonly PostGradeRepository _postGradeRepo;
        private readonly UserRatingRepository _userRatingRepository;

        public PostService(
            EfCoreExampleContext context,
            PostRepository postRepo,
            PostGradeRepository postGradeRepo,
            UserRatingRepository userRatingRepository)
        {
            _context = context;
            _postRepo = postRepo;
            _postGradeRepo = postGradeRepo;
            _userRatingRepository = userRatingRepository;
        }

        public async Task LikeAsyncV1(int postId, int userId)
        {
            await _context.Database.BeginTransactionAsync(IsolationLevel.ReadCommitted);

            var post = await _postRepo.GetByIdAsync(postId);
            var existingGrade = await _postGradeRepo.GetAsync(postId, userId);
            if (existingGrade != null && existingGrade.Value == 1)
            {
                // already liked
                _context.Database.RollbackTransaction();
                return;
            }

            // remove dislike automatically
            if (existingGrade != null)
            {
                existingGrade.Value = 1;
            }
            else
            {
                var newGrade = new PostGrade
                {
                    PostId = postId,
                    UserId = userId,
                    Value = 1
                };
                await _postGradeRepo.InsertAsync(newGrade);
            }

            // update post
            post.LikeCount++;

            // intermediate commit to save grade state
            await _context.SaveChangesAsync();

            await _userRatingRepository.UpdateRatingsAsync();

            _context.Database.CommitTransaction();
        }

        public async Task LikeAsyncV2(int postId, int userId)
        {
            await _context.Database.BeginTransactionAsync(IsolationLevel.ReadCommitted);

            bool likeInserted = false;

            while (!likeInserted)
            {
                try
                {
                    var post = await _postRepo.GetByIdAsync(postId);
                    var existingGrade = await _postGradeRepo.GetAsync(postId, userId);
                    if (existingGrade != null && existingGrade.Value == 1)
                    {
                        // already liked
                        _context.Database.RollbackTransaction();
                        return;
                    }

                    // remove dislike automatically
                    if (existingGrade != null)
                    {
                        existingGrade.Value = 1;
                    }
                    else
                    {
                        var newGrade = new PostGrade
                        {
                            PostId = postId,
                            UserId = userId,
                            Value = 1
                        };
                        await _postGradeRepo.InsertAsync(newGrade);
                    }

                    // update post
                    post.LikeCount++;

                    // intermediate commit to save grade state
                    await _context.SaveChangesAsync();

                    likeInserted = true;
                }
                catch (DbUpdateException)
                {
                    // rollback current transaction and start new one
                    _context.Database.RollbackTransaction();
                    await _context.Database.BeginTransactionAsync(IsolationLevel.ReadCommitted);
                }
            }

            await _userRatingRepository.UpdateRatingsAsync();

            _context.Database.CommitTransaction();
        }

        public async Task LikeAsyncV3(int postId, int userId)
        {
            await _context.Database.BeginTransactionAsync(IsolationLevel.ReadCommitted);

            bool likeInserted = false;

            while (!likeInserted)
            {
                try
                {
                    var existingGrade = await _postGradeRepo.GetAsync(postId, userId);
                    if (existingGrade != null && existingGrade.Value == 1)
                    {
                        // already liked
                        _context.Database.RollbackTransaction();
                        return;
                    }

                    // remove dislike automatically
                    if (existingGrade != null)
                    {
                        existingGrade.Value = 1;
                    }
                    else
                    {
                        var newGrade = new PostGrade
                        {
                            PostId = postId,
                            UserId = userId,
                            Value = 1
                        };
                        await _postGradeRepo.InsertAsync(newGrade);
                    }

                    // intermediate commit to save grade state
                    await _context.SaveChangesAsync();

                    likeInserted = true;
                }
                catch (DbUpdateException)
                {
                    // rollback current transaction and start new one
                    _context.Database.RollbackTransaction();
                    await _context.Database.BeginTransactionAsync(IsolationLevel.ReadCommitted);
                }
            }

            await _postRepo.UpdateLikeCountAsync(postId, 1);
            await _userRatingRepository.UpdateRatingsAsync();

            _context.Database.CommitTransaction();
        }
    }
}
