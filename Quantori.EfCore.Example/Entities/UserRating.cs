﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Quantori.EfCore.Example.Entities
{
    public class UserRating
    {
        public int UserId { get; set; }

        public decimal Value { get; set; }

        public decimal? PreviousValue { get; set; }

        public int Position { get; set; }

        public int? PreviousPosition { get; set; }

        public int? PositionChange { get; set; }

        public DateTimeOffset? PreviousCalculatedAt { get; set; }


        [ForeignKey("UserId")]
        public User User { get; set; }
    }
}
