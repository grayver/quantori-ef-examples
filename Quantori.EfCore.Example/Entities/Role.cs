﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Quantori.EfCore.Example.Entities
{
    public class Role : TimeTrackedEntity
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(256)]
        public string Name { get; set; }

        public string Description { get; set; }


        public ICollection<UserRole> UserRoles { get; set; }
        public ICollection<RolePermission> RolePermissions { get; set; }
    }
}
