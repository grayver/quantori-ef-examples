﻿using System;

namespace Quantori.EfCore.Example.Entities
{
    public abstract class TimeTrackedEntity
    {
        public DateTimeOffset? CreatedAt { get; set; }
        public DateTimeOffset? LastModifiedAt { get; set; }
    }
}
