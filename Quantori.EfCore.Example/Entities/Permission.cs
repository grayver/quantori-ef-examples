﻿using System.ComponentModel.DataAnnotations;

namespace Quantori.EfCore.Example.Entities
{
    public class Permission : TimeTrackedEntity
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(128)]
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
