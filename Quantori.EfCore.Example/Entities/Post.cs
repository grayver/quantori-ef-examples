﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Quantori.EfCore.Example.Entities
{
    public abstract class Post : TimeTrackedEntity
    {
        public int Id { get; set; }

        [MaxLength(32)]
        public string Type { get; set; }

        public int AuthorId { get; set; }

        [Required]
        [MaxLength(256)]
        public string Title { get; set; }

        public string Text { get; set; }

        [ConcurrencyCheck]
        public int LikeCount { get; set; }


        [ForeignKey("AuthorId")]
        public User Author { get; set; }
        public ICollection<ReadFlag> ReadFlags { get; set; }
    }
}
