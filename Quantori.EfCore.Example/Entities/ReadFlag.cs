﻿namespace Quantori.EfCore.Example.Entities
{
    public class ReadFlag
    {
        public int PostId { get; set; }
        public int UserId { get; set; }

        public Post Post { get; set; }
        public User User { get; set; }
    }
}
