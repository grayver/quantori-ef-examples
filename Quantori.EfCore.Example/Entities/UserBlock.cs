﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Quantori.EfCore.Example.Entities
{
    public class UserBlock : TimeTrackedEntity
    {
        public int UserId { get; set; }

        public int BlockId { get; set; }


        [ForeignKey("UserId")]
        public User User { get; set; }

        [ForeignKey("BlockId")]
        public User Block { get; set; }
    }
}
