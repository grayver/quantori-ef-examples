﻿using System.ComponentModel.DataAnnotations;

namespace Quantori.EfCore.Example.Entities
{
    public class Review : Post
    {
        [Required]
        [MaxLength(256)]
        public string Subject { get; set; }

        public decimal? Mark { get; set; }
    }
}
