﻿namespace Quantori.EfCore.Example.Entities
{
    public class PostGrade : TimeTrackedEntity
    {
        public int UserId { get; set; }

        public int PostId { get; set; }

        public int Value { get; set; }


        public User User { get; set; }
        public Post Post { get; set; }
    }
}
