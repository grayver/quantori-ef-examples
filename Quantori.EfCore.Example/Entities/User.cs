﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Quantori.EfCore.Example.Entities
{
    public class User : TimeTrackedEntity
    {
        public int Id { get; set; }

        [MaxLength(128)]
        public string Email { get; set; }

        public bool IsEmailConfirmed { get; set; }

        [MaxLength(256)]
        public string EmailConfirmToken { get; set; }

        public DateTimeOffset? EmailConfirmTokenExpiresAt { get; set; }

        [MaxLength(128)]
        public string Firstname { get; set; }

        [MaxLength(128)]
        public string Lastname { get; set; }

        public DateTimeOffset? RegisteredAt { get; set; }


        [NotMapped]
        public bool? IsFriend { get; set; }

        [NotMapped]
        public bool? IsOnline { get; set; }

        [NotMapped]
        public bool? IsBlocked { get; set; }


        public ICollection<UserRole> UserRoles { get; set; }
        public ICollection<UserFriend> UserFriends { get; set; }
        public UserRating Rating { get; set; }
        public ICollection<Article> AuthoredArticles { get; set; }
        public ICollection<Review> AuthoredReviews { get; set; }
        public ICollection<ReadFlag> ReadFlags { get; set; }
    }
}
